import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExitRoutingModule } from './exit-routing.module';
import { InitExitComponent } from './init-exit/init-exit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    InitExitComponent,
  ],
  imports: [
    CommonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExitRoutingModule,
    NgbModalModule
  ]
})
export class ExitModule { }
