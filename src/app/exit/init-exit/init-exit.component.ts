import { Component, OnInit } from '@angular/core';
import { StockMovement } from 'src/app/_model/stock-movement.model';
import { StockMovementLine } from 'src/app/_model/stock-movement-line.model';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { StockMovementService } from 'src/app/_service/stock-movement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from 'src/app/_service/product.service';
import { Product } from 'src/app/_model/product.model';
@Component({
  selector: 'app-init-exit',
  templateUrl: './init-exit.component.html',
  styleUrls: ['./init-exit.component.scss']
})
export class InitExitComponent implements OnInit {
  exitForm: FormGroup = new FormGroup(
    {
      designation: new FormControl(''),
      observation: new FormControl(''),
      quantity: new FormGroup(''),
      price: new FormGroup(''),

    }
  );
  lines: StockMovementLine[] = [];
  products: Product[] = [];
  productId: any;
  loading = false;
  submitted = false;
  id?: number;
  title = "Add entrance";
  constructor(
    private formBuilder: FormBuilder,
    private stockMovementService: StockMovementService,
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private modalService: NgbModal) { }
  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.productService.getProductList().subscribe(data => {
      this.products = data
    });
    this.exitForm = this.formBuilder.group(
      {
        observation: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        quantity: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        price: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        description: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(40),
          ],
        ],
      }
    );
  }
  open(content: Product) {
    this.productId = Product;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((_result) => {
      this.lines.push({
        productId: this.productId,
        quantity: this.exitForm.get('quantity')?.value,
        price: this.exitForm.get('price')?.value,
      });
      this.productId = null;
      this.exitForm.reset();
    },
      (reason) => { }
    );
  }
  save() {
    console.log(this.lines);
    this.lines = [];
    this.exitForm.reset();
  }
  destock(product: any) {
    const modalRef = this.modalService.open(InitExitComponent);
    modalRef.componentInstance.product = product;
   /* modalRef.result.then(
      (result) => this.stockMovementService.createExit(stockMovement).subscribe(data => {
        console.log(`product exited successfully: ${data}`)
        this.router.navigateByUrl("/products/reduce-stock")
      })
    )*/

  }
    
  public addStockMovementLine(productId: string,quantity: number,price: number):void{
   let line= new StockMovementLine(productId,quantity,price);
   line.productId= productId;
   line.quantity= quantity;
   line.price=price;
   this.lines.push(line);
  }
 
  get f() {
    return this.exitForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.exitForm.invalid) {
      return;
    }
    let designation = this.f["designation"].value;
    let observation = this.f["observation"].value;
    let lines=this.lines;
    let stockMovement={designation,observation,lines} 
    console.log(stockMovement);
    if (this.id) {
      this.stockMovementService.createExit( stockMovement).subscribe(data => {
      console.log(`product exited successfully: ${data}`)
      this.router.navigateByUrl("/products/reduce-stock")
    })
  }
    else {
  (error: any)=>{console.error('Erreur de destockage:',error)};
    }
  }
  onReset(): void {
    this.submitted = false;
    this.exitForm.reset();
  }
}


