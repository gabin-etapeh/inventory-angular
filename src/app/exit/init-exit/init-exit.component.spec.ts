import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitExitComponent } from './init-exit.component';

describe('InitExitComponent', () => {
  let component: InitExitComponent;
  let fixture: ComponentFixture<InitExitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitExitComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InitExitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
