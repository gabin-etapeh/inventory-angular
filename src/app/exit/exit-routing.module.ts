import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitExitComponent } from './init-exit/init-exit.component';


const routes: Routes = [
  
  {
    path: 'init-exit', 
    component: InitExitComponent
  },
  {
    path: '',
    redirectTo: 'init-exit',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExitRoutingModule { }
