import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { CategoryService } from 'src/app/_service/category.service';
import { Category } from 'src/app/_model/category.model'; 
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-add-edit-category',
  templateUrl: './add-edit-category.component.html',
  styleUrls: ['./add-edit-category.component.scss']
})
export class AddEditCategoryComponent implements OnInit{
  categoryForm: FormGroup = new FormGroup(
    {
      categoryName: new FormControl(''),
      description: new FormControl(''),
    }
  );
  loading = false;
  submitted = false;
  id?: number;
  title = "Add category";
  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private router: Router) { }
  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.categoryForm = this.formBuilder.group(
      {
        categoryName: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        description: ['',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(40),
          ],
        ],
      },
    );
    if (this.id) {
      this.title = 'Update category';
      this.loading = true;
      this.categoryService.getCategoryById(this.id)
        .subscribe(x => {
          this.categoryForm.patchValue(x);
          this.loading = false;
        });
    }
  }
  get f() {
    return this.categoryForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.categoryForm.invalid) {
      return;
    }
    let categoryName = this.f["categoryName"].value;
    let description = this.f["description"].value;
    let category = new Category(categoryName, description);
    console.log(category);
    if (this.id) {
      this.categoryService.updateCategory(this.id, category).subscribe(data => {
        console.log(`Category updated successfully: ${data}`)
        this.router.navigateByUrl("/categories")
      })
    }
    else {
      this.categoryService.createCategory(category)
        .subscribe(response => {
          console.log(response);
          this.router.navigateByUrl(`/categories`);
        });
        console.log(category)
    }
  }
  onReset(): void {
    this.submitted = false;
    this.categoryForm.reset();
  }
}
