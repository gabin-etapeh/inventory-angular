import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewCategoriesComponent } from './view-categories/view-categories.component';
import { AddEditCategoryComponent } from './add-edit-category/add-edit-category.component';

const routes: Routes = [
  {
    path: 'list-categories', 
    component: ViewCategoriesComponent
  },
  {
    path: 'add-category', 
    component: AddEditCategoryComponent
  },
  {
    path: 'edit-category/:id', 
    component: AddEditCategoryComponent
  },
  {
    path: '',
    redirectTo: 'list-categories',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
