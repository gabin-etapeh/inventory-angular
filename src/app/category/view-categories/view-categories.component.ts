import { Component, Input, OnInit } from '@angular/core';
import { Category } from 'src/app/_model/category.model';
import { CategoryService } from 'src/app/_service/category.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-categories',
  templateUrl: './view-categories.component.html',
  styleUrls: ['./view-categories.component.scss'],
  providers: [CategoryService]
})
export class ViewCategoriesComponent implements OnInit {

  category: Category[] = [];
  swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success btn-sm',
      cancelButton: 'btn btn-danger btn-sm'
    },
    buttonsStyling: false
  })
  categories: Category[] = [];
  constructor(private categoryService: CategoryService) { }
  updateCategory(id:any){
    this.categoryService.updateCategory(id)
      .subscribe(() => {
        console.log('la modifcation a reussi')
      })
  }
  deleteCategory(id: any) {
    this.categoryService.deleteCategory(id)
      .subscribe(() => {
        console.log('la suppression a reussi')
        this.ngOnInit()
      })
  }

  ngOnInit(): void {
    this.categoryService.getCategoryList().subscribe(data => {
      this.categories = data
    });
  }
  initDeleteCategory(category: Category) {
    this.swalWithBootstrapButtons.fire({
      title: 'Suppression d\'une catégorie',
      text: `Voullez-vous supprimer la catégorie ${category.categoryName}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteCategory(category.id)
        this.swalWithBootstrapButtons.fire(
          'Suppression d\'une catégorie!',
          `La catégorie ${category.categoryName} a été supprimé avec succès.`,
          'success'
        )
      }
    })
  }
}



