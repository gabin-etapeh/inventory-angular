import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Provider } from 'src/app/_model/provider.model';
import { ProviderService } from 'src/app/_service/provider.service';
@Component({
  selector: 'app-add-edit-provider',
  templateUrl: './add-edit-provider.component.html',
  styleUrls: ['./add-edit-provider.component.scss']
})
export class AddEditProviderComponent implements OnInit {
  providerForm: FormGroup = new FormGroup(
    {
      name: new FormControl(''),
      description: new FormControl(''),
      phone: new FormControl(''),
      city: new FormControl
    }
  );
  loading = false;
  submitted = false;
  id?: number;
  title = "Add category";
  constructor(
    private formBuilder: FormBuilder,
    private providerService: ProviderService,
    private route: ActivatedRoute,
    private router: Router) { }
  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.providerForm = this.formBuilder.group(
      {
        name: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        description: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(40),
          ],
        ],
        phone: ['',
          [
            Validators.required,
            Validators.minLength(9),
            Validators.maxLength(20),
          ],
        ],
        city: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
      },
    );
    if (this.id) {
      this.title = 'Update category';
      this.loading = true;
      this.providerService.getProviderById(this.id)
        .subscribe(x => {
          this.providerForm.patchValue(x);
          this.loading = false;
        });
    }
  }
  get f() {
    return this.providerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.providerForm.invalid) {
      return;
    }
    let name = this.f["name"].value;
    let description = this.f["description"].value;
    let phone = this.f["phone"].value;
    let city = this.f["city"].value;
    let provider = new Provider(name,description,phone,city);
    console.log(provider);
    if (this.id) {
      this.providerService.updateProvider(this.id, provider).subscribe(data => {
      console.log(`Provider updated successfully: ${data}`)
      this.router.navigateByUrl("/suppliers")
    })
  }
    else {
  this.providerService.createProvider(provider)
    .subscribe(response => {
      console.log(response);
      this.router.navigateByUrl(`/supppliers`);
    });
  console.log(provider)
}
  }
onReset(): void {
  this.submitted = false;
  this.providerForm.reset();
}
}