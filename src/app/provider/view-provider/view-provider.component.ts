import { Component, OnInit } from '@angular/core';
import { Provider } from 'src/app/_model/provider.model';
import { ProviderService } from 'src/app/_service/provider.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-provider',
  templateUrl: './view-provider.component.html',
  styleUrls: ['./view-provider.component.scss']
})
export class ViewProviderComponent implements OnInit { 
  providers: Provider[] = [];
  swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success btn-sm',
      cancelButton: 'btn btn-danger btn-sm'
    },
    buttonsStyling: false
  })
constructor (private providerService: ProviderService){}
deleteProvider(id: any) {
  this.providerService.deleteProvider(id)
    .subscribe(() => {
      console.log('la suppression a reussi')
      this.ngOnInit()
    })
}
ngOnInit(): void {
  this.providerService.getProviderList().subscribe(data => {
    this.providers = data
  });
   }
   initDeleteProvider(provider: Provider) {
    this.swalWithBootstrapButtons.fire({
      title: 'Suppression d\'un fournisseur',
      text: `Voullez-vous supprimer le fournisseur ${provider.name}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteProvider(provider.id)
        this.swalWithBootstrapButtons.fire(
          'Suppression d\'un fournisseur!',
          `Le fournisseur ${provider.name} a été supprimé avec succès.`,
          'success'
        )
      }
    })
  }
  }


