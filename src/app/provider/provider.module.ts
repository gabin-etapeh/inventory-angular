import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderRoutingModule } from './provider-routing.module';
import { AddEditProviderComponent } from './add-edit-provider/add-edit-provider.component';
import { ViewProviderComponent } from './view-provider/view-provider.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddEditProviderComponent,
    ViewProviderComponent
  ],
  imports: [
    CommonModule,
    ProviderRoutingModule,
    FormsModule,
ReactiveFormsModule,
]
})
export class ProviderModule { }
