import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditProviderComponent } from './add-edit-provider/add-edit-provider.component';
import { ViewProviderComponent } from './view-provider/view-provider.component';

const routes: Routes = [
  {
    path: 'add-edit-provider', component: AddEditProviderComponent
  },
  {
    path: 'view-provider', component: ViewProviderComponent
  },
  {
    path: '', redirectTo: 'view-provider', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRoutingModule { }
