export class Provider {
    id?:string;
     name: string;
     description: string;
     phone : string;
     city: string;
     constructor(name: string, description: string,phone:string,city:string) {
        this.name = name;
        this.description = description;   
        this.phone=phone;
        this.city=city;
    }
}