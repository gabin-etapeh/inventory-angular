import { StockMovementLine } from "./stock-movement-line.model";

export class StockMovement {
    
    id?: string;
    designation: string;
    observation: string;
    lines: StockMovementLine [];

    constructor(
               designation: string,
               observation: string,
               lines: StockMovementLine []) {
        this.designation = designation; 
        this.observation = observation;
        this.lines = lines;  
    }

}  
