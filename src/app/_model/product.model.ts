import { Category } from "./category.model";

export class Product {
    id?: string;
    name: string;
    quantity: string;
    stockAlert: number;
    price: number;
    barCode: string;
    imageUrl: string;
    variety: string;
    expiryDate: Date;
    description: string;
    categoryId: Category;
    unit: string;

    constructor(name: string,
        quantity: string,
        stockAlert: number,
        price: number,
        barCode: string,
        imageUrl: string,
        variety: string,
        expiryDate: Date,
        description: string,
        categoryId: Category,
        unit: string) {
        this.name = name;
        this.quantity = quantity;
        this.stockAlert = stockAlert;
        this.price = price;
        this.barCode = barCode;
        this.imageUrl = imageUrl;
        this.variety = variety;
        this.expiryDate = expiryDate;
        this.description = description;
        this.categoryId = categoryId;
        this.unit = unit;

    }
}