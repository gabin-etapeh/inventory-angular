export class Category {
        id?: string;
        categoryName: string;
        description: string;
        
        constructor(categoryName: string, description: string) {
            this.categoryName = categoryName;
            this.description = description;   
        }
    
    }  
