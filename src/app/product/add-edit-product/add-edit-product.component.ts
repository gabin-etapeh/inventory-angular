import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/_model/product.model';
import { ProductService } from 'src/app/_service/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from 'src/app/_service/category.service';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Category } from 'src/app/_model/category.model';
@Component({
  selector: 'app-add-edit-product',
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.scss']
})
export class AddEditProductComponent implements OnInit {
  categories: Category[] = [];

  productForm: FormGroup = new FormGroup(
    {
      name: new FormControl(''),
      quantity: new FormControl(''),
      stockAlert: new FormControl(''),
      price: new FormControl(''),
      barCode: new FormControl(''),
      imageUrl: new FormControl(''),
      variety: new FormControl(''),
      expiryDate: new FormControl(''),
      description: new FormControl(''),
      catagoryId: new FormControl(''),
      unit: new FormControl(''),


    }
  );
  loading = false;
  submitted = false;
  id?: number;
  title = "Add product";
  constructor(private formBuilder: FormBuilder,
    private productService: ProductService,
    private categoryService: CategoryService,

    private route: ActivatedRoute,
    private router: Router) { }
  ngOnInit() {
    this.categoryService.getCategoryList().subscribe(data => {
      this.categories = data
    });
    this.id = this.route.snapshot.params['id'];
    this.productForm = this.formBuilder.group(
      {

        name: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        quantity: ['',
          [
            Validators.required,
            Validators.maxLength(20),
          ],
        ],
        stockAlert: ['',
          [
            Validators.required,

          ],
        ],
        price: ['',
          [
            Validators.required,
            Validators.maxLength(20),
          ],
        ],
        barCode: ['',
          [
            Validators.required,
            Validators.maxLength(20),
          ],
        ],
        imageUrl: ['',
          [
          ],
        ],
        variety: ['',
          [
            Validators.required,
            Validators.maxLength(20),
          ],
        ],
        expiryDate: ['',
          [
            Validators.required,
            Validators.maxLength(20),
          ],
        ],
        description: ['',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(40),
          ],
        ],
        categoryId: ['',
          [
            Validators.required,
          ],
        ],
        unit: ['',
          [

            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
      },
    );
    if (this.id) {
      this.title = 'Update product';   
    }
  }

  get f() {
    return this.productForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.productForm.invalid) {
      return;
    }
    let name = this.f["name"].value;
    let quantity = this.f["quantity"].value;
    let stockAlert = this.f["stockAlert"].value;
    let price = this.f["price"].value;
    let barCode = this.f["barCode"].value;
    let imageUrl = this.f["imageUrl"].value;
    let variety = this.f["variety"].value;
    let expiryDate = this.f["expiryDate"].value;
    let description = this.f["description"].value;
    let categoryId = this.f["categoryId"].value;
    let unit = this.f["unit"].value;
    let product = new Product(name, quantity, stockAlert, price, barCode, imageUrl, variety, expiryDate, description, categoryId, unit);
    console.log(product);
    if (this.id) {
      this.productService.updateProduct(this.id, product).subscribe(data => {
        console.log(`Product updated successfully: ${data}`)
        this.router.navigateByUrl("/products")
      })
    }
    else {
      this.productService.createProduct(product)
        .subscribe(response => {
          console.log(response);
          this.router.navigateByUrl(`/products`);
        });
      console.log(product)
    }
  }
  onReset(): void {
    this.submitted = false;
    this.productForm.reset();
  }

}
