import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/_model/product.model';
import { ProductService } from 'src/app/_service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {

  filterValue!: string;
  products: Product[] = [];
  swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success btn-sm',
      cancelButton: 'btn btn-danger btn-sm'
    },
    buttonsStyling: false
  })
  searchText!: string;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.getProductList().subscribe(data => {
      this.products = data
    });
  }

  initDeleteProduct(product: Product) {
    this.swalWithBootstrapButtons.fire({
      title: 'Suppression d\'un produit',
      text: `Voullez-vous supprimer le produit ${product.name}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteProduct(product.id)
        this.swalWithBootstrapButtons.fire(
          'Suppression d\'un produit!',
          `Le produit ${product.name} a été supprimé avec succès.`,
          'success'
        )
      }
    })
  }
  deleteProduct(productId: string | undefined) {
    if (productId == undefined) {
      console.log('Aucun identifiant défini pour le produit à supprimer')
      return
    }
    this.productService.deleteProduct(productId)
      .subscribe(() => {
        console.log('La suppression a reussi')
        this.ngOnInit()
      })
  }

  onFilterChange($event: any) {
    //console.log(this.filterValue)
    if (this.filterValue == "all") {
      this.productService.getProductList().subscribe(data => {
        this.products = data
        console.log(this.products)
      });

    } else if (this.filterValue == "expired") {
      this.productService.getExpiredProducts().subscribe(data => {
        this.products = data
        console.log(this.products)

      });

    } else if (this.filterValue == "low") {
      this.productService.getBellowStockProducts().subscribe(respose => {
        this.products = respose.data
        console.log(this.products)
      });
    }
    // TODO: c'est ici maintenant que tu dois maintenant filtrer la liste des produits (this.products)
    // pour ne garder que ceux qui respecte l'une des conditions du filtre
    // 1. si c'est 'all' la valeur de this.filterValue => tu garde la liste de base
    // 2. si c'est 'lowStock' la valeur de this.filterValue => tu ne garde que les produits en rupture de stock
    // 3. si c'est 'expired' la valeur de this.filterValue => tu ne garde que les produits périmés

  }
  getProductByKeyword($event: any) {
    if (this.searchText == "") {
      this.productService.getProductList().subscribe(data => {
        this.products = data
        console.log(this.products)
      });
    } else {
      console.log(this.searchText)
      let keyWord = this.searchText.toLocaleLowerCase()
      let searchProducts = this.products.filter(
        product => product.name.toLocaleLowerCase().includes(keyWord)
          || product.variety.toLocaleLowerCase().includes(keyWord)
          || product.description.toLocaleLowerCase().includes(keyWord));
      this.products = searchProducts
      /*this.productService.getProductByKeyword(this.searchText).subscribe(data => {
        this.products = data
        console.log(this.products)
      });*/
    }
  }
}
