import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditProductComponent } from './add-edit-product/add-edit-product.component';
import { ViewProductComponent } from './view-product/view-product.component';
import { ListProductsComponent } from './list-products/list-products.component';

const routes: Routes = [
  {
    path: 'list-products', component: ListProductsComponent
  },
  {
    path: 'add-product', component: AddEditProductComponent
  },
  {
    path: 'edit-product/:id', component: AddEditProductComponent
  },
  {
    path: 'view-product', component: ViewProductComponent
  },
  {
    path: '', redirectTo: 'list-products', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
