import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Category } from "../_model/category.model";
import { Observable } from "rxjs";
@Injectable({
   providedIn: 'root'
})
export class CategoryService {
   constructor(private http: HttpClient) { }

   private baseUrl = 'http://localhost:8080';

   public getCategoryList(): Observable<Category[]> {
      return <Observable<Category[]>>this.http.get(`${this.baseUrl}/categories`)
   }

   createCategory(category: Category): Observable<Category> {
      return <Observable<Category>>this.http.post(`${this.baseUrl}/category`, category);
   }
   deleteCategory(id: any): Observable<any> {
      return <Observable<any>>this.http.delete(`${this.baseUrl}/category/${id}`)
   }
   updateCategory(id?: number, category?: Category): Observable<Category> {
      return <Observable<Category>>this.http.put(`${this.baseUrl}/category/${id}`, category);
   }
   getCategoryById(id?: number): Observable<Category> {
      return <Observable<Category>>this.http.get(`${this.baseUrl}/category/${id}`);
   }
}
