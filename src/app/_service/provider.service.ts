import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Provider } from "../_model/provider.model";
import { Observable } from "rxjs";
@Injectable({
   providedIn: 'root'
})
export class ProviderService {
   constructor(private http: HttpClient) { }
   private baseUrl = 'http://localhost:8080';
   public getProviderList(): Observable<Provider[]> {
    return <Observable<Provider[]>>this.http.get(`${this.baseUrl}/suppliers`)
 }
 createProvider(provider: Provider): Observable<Provider> {
    return <Observable<Provider>>this.http.post(`${this.baseUrl}/supplier`, provider);
 }
 deleteProvider(id: any): Observable<any> {
    return <Observable<any>>this.http.delete(`${this.baseUrl}/supplier/${id}`)
 }
 updateProvider(id?: number, provider?: Provider): Observable<Provider> {
    return <Observable<Provider>>this.http.put(`${this.baseUrl}/supplier/${id}`, provider);
 }
 getProviderById(id?: number): Observable<Provider> {
    return <Observable<Provider>>this.http.get(`${this.baseUrl}/supplier/${id}`);
 }
}