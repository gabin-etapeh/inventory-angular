import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StockMovement } from "../_model/stock-movement.model";
import { Observable } from "rxjs";
@Injectable({
   providedIn: 'root'
})
export class StockMovementService {
   constructor(private http: HttpClient) { }

   private baseUrl = 'http://localhost:8080';

   createEntrance(stockMovement: StockMovement): Observable<StockMovement> {
      return <Observable<StockMovement>>this.http.post(`${this.baseUrl}/products/add-stock`, stockMovement);
   }
   createExit(stockMovement: StockMovement): Observable<StockMovement> {
    return <Observable<StockMovement>>this.http.post(`${this.baseUrl}/products/reduce-stock`, stockMovement);
 }
}
