import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Product } from "../_model/product.model";
import { Observable } from "rxjs";
import { Category } from "../_model/category.model";
import { StockMovement } from "../_model/stock-movement.model";
@Injectable({
   providedIn: 'root'
})
export class ProductService {
   destockProduct(id: any, quantity: any) {
     throw new Error('Method not implemented.');
   }
   constructor(private http: HttpClient) { }

   private baseUrl = 'http://localhost:8080';

   public getProductList(): Observable<Product[]> {
      return <Observable<Product []>>this.http.get(`${this.baseUrl}/products`)
   }
   createProduct(product: Product): Observable<Product> {
      return <Observable<Product>>this.http.post(`${this.baseUrl}/product`, product);
   }
   deleteProduct(id: any): Observable<any> {
      return <Observable<any>>this.http.delete(`${this.baseUrl}/product/${id}`)
   }
   updateProduct(id?: number, product?: Product): Observable<Product> {
      return <Observable<Product>>this.http.put(`${this.baseUrl}/product/${id}`, product);
   }
   getProductByKeyword(keyWord?: string): Observable<Product []> {
      return <Observable<Product []>>this.http.get(`${this.baseUrl}/products/search?keyword=${keyWord}`);
   }
   getExpiredProducts(): Observable<Product[]> {
      return <Observable<Product []>>this.http.get(`${this.baseUrl}/products/expired`);
   }
   getBellowStockProducts(): Observable<any> {
      return <Observable<any>>this.http.get(`${this.baseUrl}/products/bellowStock`);
   }
   public getCategoryList(): Observable<Category[]> {
      return <Observable<Category[]>>this.http.get(`${this.baseUrl}/categories`)
   }
   createExit(stockMovement: StockMovement): Observable<StockMovement> {
      return <Observable<StockMovement>>this.http.post(`${this.baseUrl}/products/reduce-stock`, stockMovement);
   }
}
